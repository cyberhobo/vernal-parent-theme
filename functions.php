<?php
/*
 * Common functionality in Vernal themes
 */

add_action( 'init', array( 'Vernal', 'action_init' ) );
add_action( 'admin_init', array( 'Vernal', 'action_admin_init' ) );
add_action( 'wp_ajax_vernal_query', array( 'Vernal', 'ajax_vernal_query' ) );
add_action( 'wp_ajax_nopriv_vernal_query', array( 'Vernal', 'ajax_vernal_query' ) );

class Vernal {

	static $impersonate_page_id;

	static function action_init() {
		add_action( 'template_redirect', array( __CLASS__, 'action_template_redirect' ) );
		add_filter( 'template_include', array( __CLASS__, 'filter_template_include' ) );

		wp_register_script( 'tipsy', path_join( get_template_directory_uri(), 'scripts/jquery.tipsy.js' ), array( 'jquery' ), false, true );
		wp_register_style( 'tipsy', path_join( get_template_directory_uri(), 'scripts/tipsy.css' ) );
		wp_register_script( 'hoverintent', path_join( get_template_directory_uri(), 'scripts/hoverIntent.js' ), array( 'jquery' ), false, true );
		wp_register_script( 'superfish', path_join( get_template_directory_uri(), 'scripts/superfish.js' ), array( 'jquery' ), '1.4.8', true );
		wp_register_script( 'vernal-quicktabs', path_join( get_template_directory_uri(), 'scripts/quicktabs.js' ), array( 'jquery' ), false, true );
		wp_register_script( 'vernal-expando', path_join( get_template_directory_uri(), 'scripts/expando.js' ), array( 'jquery' ), false, true );
		wp_register_script( 'vernal-slideshow', path_join( get_template_directory_uri(), 'scripts/slideshow.js' ), array( 'jquery' ), false, true );
		wp_register_script( 'vernal-hashscroll', path_join( get_template_directory_uri(), 'scripts/hashscroll.js' ), array( 'jquery' ), false, true );
	}

	static function action_admin_init() {
		add_action( 'admin_enqueue_scripts', array( __CLASS__, 'action_admin_enqueue_scripts' ) );
	}

	static function action_template_redirect() {
		// Headers have usually not yet been sent, so a redirect is possible
		// We want to direct attachment page requests to the actual attachment file by default
		if ( is_attachment() and apply_filters( 'vernal_redirect_attachments', true ) ) {
			$attachment = get_queried_object();
			wp_redirect( wp_get_attachment_url( $attachment->ID ) );
		}
	}

	static function filter_template_include( $template ) {
		// If there is a *.js or *.css with the same name as the template, queue it
		$script = str_replace( '.php', '.js', $template );
		if ( is_readable( $script ) ) {
			$script_uri = path_join( get_stylesheet_directory_uri(), basename( $script ) );
			$handle = 'vernal-auto-' . sanitize_title( basename( $script, '.js' ) );
			wp_enqueue_script( $handle, $script_uri, array(), false, true );
		}
		$stylesheet = str_replace( '.php', '.css', $template );
		if ( is_readable( $stylesheet ) ) {
			$stylesheet_uri = path_join( get_stylesheet_directory_uri(), basename( $stylesheet ) );
			$handle = 'vernal-auto-' . sanitize_title( basename( $stylesheet, '.css' ) );
			wp_enqueue_style( $handle, $stylesheet_uri );
		}
		return $template;
	}

	static function in_subcategory( $cat_id ) {
		$subcats = get_term_children( $cat_id, 'category' );
		if ( $subcats && !is_wp_error( $subcats ) ) {
			foreach( $subcats as $child_category ) {
				if( in_category($child_category) ) {
					return true;
				}
			}
		}
		return false;
	}

	static function is_child_of( $page_id, $potential_child_id = '' ) {

		$is_child = false;

		if ( ! is_int( $page_id ) ) {
			$page = get_page_by_path( $page_id );
			$page_id = empty( $page ) ? 0 : $page->ID;
		}

		if ( empty( $potential_child_id ) ) { 
			$potential_child_id = get_the_ID();
		}

		$ancestors = get_post_ancestors( $potential_child_id );

		if ( ! empty( $ancestors ) ) {
			$is_child = in_array( $page_id, $ancestors );
		}

		return $is_child;
	}

	static function start_page_impersonation() {
		if ( self::$impersonate_page_id ) {
			query_posts( array( 'page_id' => self::$impersonate_page_id ) );
			if ( have_posts() ) 
				the_post();
		}
	}

	static function end_page_impersonation() {
		if ( self::$impersonate_page_id ) 
			wp_reset_query();
	}

	static function action_admin_enqueue_scripts() {
		$admin_css_path = path_join( get_stylesheet_directory(), 'admin.css' );
		if ( is_readable( $admin_css_path ) ) 
			wp_enqueue_style( 'vernal-admin', path_join( get_stylesheet_directory_uri(), 'admin.css' ) );
	}

	/**
	* Respond to queries for document HMTL fragments via AJAX.
	* e.g. http://site.com/wp-admin/admin-ajax.php?action=vernal_query&post_type=custom&posts_per_page=3
	* 
	* Called via actions wp_ajax_vernal_query and wp_ajax_nopriv_vernal_query
	*/
	static function ajax_vernal_query() {
		$template = array();
		if ( isset( $_REQUEST['template'] ) ) {
			if ( isset( $_REQUEST['template_part'] ) ) {
				$template[] = $_REQUEST['template'] . '-' . $_REQUEST['template_part']. '.php';
				unset( $_REQUEST['template_part'] );
			}
			$template[] = $_REQUEST['template'] . '.php';
			unset( $_REQUEST['template'] );
		}
		$template[] = 'index.php';

		$query_args = $_REQUEST;

		// Explode list parameters
		foreach( $query_args as $name => $value ) {
			if ( strpos( $name, '__in' ) or strpos( $name, '__not' ) or strpos( $name, '__and' ) ) {
				$query_args[$name] = explode( ',', $value );
			}
		}

		query_posts( $query_args );
		locate_template( $template, true );
		exit();
	}

	static function text_terms( $id = 0, $taxonomy, $before = '', $sep = ', ', $after = '', $args = '' ) {
		$default_args = array( 'wrap_tag' => '', 'wrap_class_field' => 'slug', 'field' => 'name' );
		$args = wp_parse_args( $args, $default_args );
		/** @var $wrap_tag */
		/** @var $wrap_class_field */
		/** @var $field */
		extract( $args );

		$terms = get_the_terms( $id, $taxonomy );

		if ( is_wp_error( $terms ) )
			return $terms;

		if ( empty( $terms ) )
			return false;

		if ( $wrap_tag ) 
			$items = array_map( create_function( '$item', 'return "<' . $wrap_tag . ' class=\"term_{$item->' . $wrap_class_field . '}\">{$item->name}</' . $wrap_tag . '>";' ), $terms );
		else 
			$items = wp_list_pluck( $terms, $field );
		return $before . join( $sep, $items ) . $after;
	}

	static function truncate_text( $text, $args = '' ) {
		$default_args = array(
			'length' => 100,
			'more' => '...'
		);
		$args = wp_parse_args( $args, $default_args );
		/** @var $length */
		/** @var $more */
		extract( $args );
		if ( strlen( $text ) > $length ) {
			$text = substr( $text, 0, $length ) . $more;
		}
		return $text;
	}

	static function get_root_id() {
		global $post;
		$root_page_id = null;
		if ( $post ) 
			$root_page_id = ( empty( $post->ancestors ) ) ? $post->ID : end( $post->ancestors );
		return $root_page_id;
	}

	static function get_level() {
		global $post;
		$level = null;
		if ( $post and isset( $post->ancestors ) )
			$level = count( $post->ancestors ) + 1;
		return $level;
	}

	static function in_ancestors( $check_post ) {
		global $post;
		if ( is_object( $check_post ) ) 
			$post_id = $check_post->ID;
		else
			$post_id = absint( $check_post );
			
		if ( $post && isset( $post->ancestors ) )
			return in_array( $post_id, $post->ancestors );

		return false;
	}

	/**
	 * Echo or return a value based on a flag.
	 */
	static function echo_or_string( $value, $echo ) {
		if ( !$echo )
			return $value;
		echo $value;
		return '';
	}

	/**
	* Get a request variable or default fallback.
 	*/
	static function req( $name, $default = '', $echo = true ) {
		$value = ( isset( $_REQUEST[$name] ) ? $_REQUEST[$name] : $default ); 
		return self::echo_or_string( $value, $echo );
	}

	/**
	* Get the current request URL.
	*/
	static function get_current_url( $args = '' ) {
		$default_args = array( 
			'include_query_string' => true
		);
		$args = wp_parse_args( $args, $default_args );
		/** @var $include_query_string */
		extract( $args );
		
		if ( empty( $_SERVER['HTTPS'] ) )
			$url = 'http://';
		else 
			$url = 'https://';

		$url .= $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

		if ( !$include_query_string ) 
			$url = str_replace( '?' . $_SERVER['QUERY_STRING'], '', $url );

		return $url;
	}

	/**
	* Ouput meta data for a post.
	*/
	static function the_post_meta( $key, $args = '' ) {
		$default_args = array( 
			'post_id' => 0,
			'echo' => true,
			'before' => '',
			'separator' => ', ',
			'after' => '',
		);
		$args = wp_parse_args( $args, $default_args );
		/** @var $post_id */
		/** @var $echo */
		/** @var $before */
		/** @var $separator */
		/** @var $after */
		extract( $args );

		if ( !$post_id )
			$post_id = get_the_ID();

		$values = get_post_meta( $post_id, $key );

		if ( empty( $values ) )
			return '';

		$output = $before . implode( $separator, $values ) . $after;
		return self::echo_or_string( $output, $echo );
	}

	/**
	* Ouput an impersonation-aware "breadcrumb" trail.
	*/
	static function impersonated_breadcrumbs( $args = '' ) {
		$default_args = array( 
			'echo' => true,
			'include_home' => true,
			'before' => '<p class="breadcrumbs">',
			'separator' => ' &gt; ',
			'after' => '</p>',
		);
		$args = wp_parse_args( $args, $default_args );
		/** @var $echo */
		/** @var $include_home */
		/** @var $before */
		/** @var $separator */
		/** @var $after */
		extract( $args );

		$crumbs = array();

		if ( $include_home )
			$crumbs[] = array( 'url' => home_url(), 'text' => 'Home' );

		if ( self::$impersonate_page_id ) {
			$leaf_object = get_queried_object();
			$queried_object = get_post( self::$impersonate_page_id );
		} else {
			$leaf_object = null;
			$queried_object = get_queried_object();
		}
		
		if ( isset( $queried_object->post_type ) ) {
			// post type archive
			$post_type_object = get_post_type_object( $queried_object->post_type );
			if ( $post_type_object->has_archive )
				$crumbs[] = array(
					'url' => get_post_type_archive_link( $queried_object->post_type ),
					'text' => $post_type_object->labels->all_items,
				);

			// Post hierarchy
			$ancestor_ids = array_reverse( get_ancestors( $queried_object->ID, $queried_object->post_type ) );
			foreach( $ancestor_ids as $ancestor_id ) {
				$ancestor = get_post( $ancestor_id );
				$crumbs[] = array( 
					'url' => get_permalink( $ancestor->ID ),
					'text' => $ancestor->post_title
				);
			}
			$crumbs[] = array( 
				'url' => get_permalink( $queried_object->ID ),
				'text' => $queried_object->post_title
			);

		} else if ( !empty( $queried_object->has_archive ) ) {
			// post type archive
			$crumbs[] = array(
				'url' => get_post_type_archive_link( $queried_object->name ),
				'text' => $queried_object->labels->all_items,
			);
		}

		if ( $leaf_object ) {
			if ( isset( $leaf_object->post_title ) )
				$crumbs[] = array( 'text' => $leaf_object->post_title  );
			else if ( isset( $leaf_object->name ) )
				$crumbs[] = array( 'text' => $leaf_object->name  );
			else if ( isset( $leaf_object->display_name ) )
				$crumbs[] = array( 'text' => $leaf_object->display_name  );
		}

		$assembled_crumbs = array();
		for( $i = 0; $i < count( $crumbs ) - 1; $i += 1 ) {
			$assembled_crumbs[] = '<a href="' . $crumbs[$i]['url'] . '">' . 
				$crumbs[$i]['text'] . '</a>';
		}
		$leaf_crumb = array_pop( $crumbs );
		$assembled_crumbs[] = '<strong>' . $leaf_crumb['text'] . '</strong>';

		$output = $before . implode( $separator, $assembled_crumbs ) . $after;
		return self::echo_or_string( $output, $echo );
	}

	/**
	* Return the URL of a randomly chosen theme file.
	*/
	static function random_file_url( $path ) {
		$dir = path_join( get_stylesheet_directory(), $path );
		$files = glob($dir . '/*.*');
		$file = array_rand($files);
		$basename = basename( $files[$file] );
		return path_join( get_stylesheet_directory_uri(), path_join( $path, $basename ) );
	}

	/**
	* Call a function or method with the given arguments if it exists.
	* 
	* Shortcut for is_callable( $func ) ? $func( ... ) : false;
	* Use to wrap plugin calls that would otherwise stop script execution if the plugins is not active.
	*
	* @param callable $callback What to call.
	* @param mixed ... Optional additional arguments which are passed on to the functions hooked to the action.
	* @return mixed False if the function does not exist, otherwise the function return value. 
	**/
	static function call_if_exists( $callback ) {
		if ( ! is_callable( $callback ) )
			return false;

		$args = array();
		for ( $a = 1; $a < func_num_args(); $a++ )
			$args[] = func_get_arg($a);

		return call_user_func_array( $callback, $args );
	}

	/**
	 * Render a template with an array of data in scope.
	 */
	static function render_template( $template_names, $data, $echo = true ) {
		$template = locate_template( $template_names );
		$output = '';
		if ( $template ) {
			extract( $data );
			if ( !$echo )
				ob_start();
			require( $template );
			if ( !$echo )
				$output = ob_get_clean();
		}
		return $output;
	}

	static function time_since( $timestamp ) {

		/* Convert to to timestamp if necesssary */
		$timestamp = is_string( $timestamp ) ? strtotime( $timestamp ) : $timestamp;

		// Common time periods as an array of arrays
		$periods = array(
			array(60 * 60 * 24 * 365 , 'year'),
			array(60 * 60 * 24 * 30 , 'month'),
			array(60 * 60 * 24 * 7, 'week'),
			array(60 * 60 * 24 , 'day'),
			array(60 * 60 , 'hour'),
			array(60 , 'minute'),
		);

		$today = time();
		$since = $today - $timestamp; // Find the difference of time between now and the past

		// Loop around the periods, starting with the biggest
		for ($i = 0, $j = count($periods); $i < $j; $i++) {
			$seconds = $periods[$i][0];
			$name = $periods[$i][1];

			// Find the biggest whole period
			if (($count = floor($since / $seconds)) != 0) {
				break;
			}
		}

		$output = ($count == 1) ? '1 '.$name : "$count {$name}s";

		if ($i + 1 < $j) {
			// Retrieving the second relevant period
			$seconds2 = $periods[$i + 1][0];
			$name2 = $periods[$i + 1][1];

			// Only show it if it's greater than 0
			if (($count2 = floor(($since - ($seconds * $count)) / $seconds2)) != 0) {
				$output .= ($count2 == 1) ? ', 1 '.$name2 : ", $count2 {$name2}s";
			}
		}
		return $output;
	}

} // End class Vernal

add_filter( 'template_include', array( 'Vernal_Wrapping', 'wrap' ), 99 );

class Vernal_Wrapping {
	/**
	* Stores the full path to the main template file
	*/
	static $main_template;

	/**
	* Stores the base name of the template file; e.g. 'page' for 'page.php' etc.
	*/
	static $base;

	static function wrap( $template ) {
		self::$main_template = $template;

		self::$base = substr( basename( self::$main_template ), 0, -4 );

		if ( 'index' == self::$base )
			self::$base = false;

		$templates = array( 'wrapper.php' );

		if ( self::$base )
			array_unshift( $templates, sprintf( 'wrapper-%s.php', self::$base ) );

		return locate_template( $templates );
	}

	/**
	* Get a sidebar that looks for a wrapper first.
	*/
	static function get_sidebar( $name = null ) {
		do_action( 'get_sidebar', $name );

		$templates = array();
		if ( isset($name) ) 
			$templates[] = "wrapper-sidebar-{$name}.php";

		$templates[] = 'wrapper-sidebar.php';

		if ( isset($name) ) 
			$templates[] = "sidebar-{$name}.php";

		$templates[] = 'sidebar.php';

		// Backward compat code will be removed in a future release
		if ('' == locate_template($templates, true))
			load_template( ABSPATH . WPINC . '/theme-compat/sidebar.php');
	}

}

