<?php $base = Vernal_Wrapping::$base; ?>
<?php /* Be careful to avoid white space above the header */ ?>
<?php get_header( $base ); ?>

<div id="page">

<?php Vernal_Wrapping::get_sidebar( $base ); ?>

<div id="page_main" class="content">

<?php include Vernal_Wrapping::$main_template; ?>

</div><!-- End page_main -->

</div><!-- End page -->

<?php get_footer( $base ); ?>
