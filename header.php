<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta content="IE=8" http-equiv="X-UA-Compatible" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<style type="text/css" media="screen">
@import url( <?php bloginfo('stylesheet_url'); ?> );
</style>
<title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<meta http-equiv="Content-Style-Type" content="text/css" />

<?php wp_head(); ?>

</head>

<body <?php body_class('paged'); ?>>

<div id="wrapper">
<div id="header">
<h1><a href="<?php echo home_url(); ?>" title=""><span><?php bloginfo( 'name' ); ?></span></a></h1>

<ul id="header_nav">
<li><?php wp_loginout($_SERVER['REQUEST_URI']); ?></li>
</ul>

<?php Vernal::start_page_impersonation(); ?>
<ul id="nav">
<li id="nav_home"><a href="<?php echo home_url(); ?>" title="">Home</a></li>
</ul>
<?php Vernal::end_page_impersonation(); ?>

<!-- End nav -->

</div>
<!-- End header -->
