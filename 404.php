<h1>We're Sorry</h1>

<p><strong>We can't find the page you were looking for. Perhaps searching can help.</strong></p>

<?php get_search_form(); ?>

<h3>Or <a href="<?php echo home_url(); ?>" title="">Try the home page</a></h3>
