(function( $ ) {

	$.fn.expando = function() {
		var $triggers = this;

		return $triggers.click( function() {
			var $trigger = $(this);
			var $target = $trigger.next();
			if ( $target.is(':visible') ) {
				$target.slideUp( 'fast', function() {
					$trigger.removeClass( 'active' );
					$target.removeClass( 'active' );
				} );
			} else {
				// Close other open items
				$triggers.filter( '.active' ).click();
				// Open this one
				$target.slideDown( 'fast', function() {
					$trigger.addClass( 'active' );
					$target.addClass( 'active' );
				} );
			}
			return false;
		} );
	};

	// Back compatibility - a default class on load
	$( function() {
		// Activate slide-next elements
		$( '.expand_next' ).expando();
	} );

}( jQuery ));
