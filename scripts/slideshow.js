jQuery(function ($) {
	var $slideshow = $('#slideshow'),
		$featured_links = $('.featured_link'),
		$featured_link, $featured_infos = $('.feature_info').hide(0),
		$next_links = $( '.featured_next_link' ),
		$prev_links = $( '.featured_previous_link' ),
		content_index = -1,
		fade_in_index = -1,
		playing = false,
		slide_milliseconds = 9000,
		fade_in_speed = 'slow',
		fade_out_speed = 'slow',
		timeout,

		incremented_index = function( increment ) {
			if ( typeof increment !== 'number' ) {
				increment = 1;
			}
			while ( increment < 0 ) {
				increment = $featured_infos.length + increment;
			}
			return ( content_index + increment ) % $featured_infos.length;
		},

		toggle_play = function (force_status) {
			if (typeof force_status === 'boolean') {
				playing = force_status;
			} else {
				playing = !playing;
			}
			if (timeout) {
				clearTimeout(timeout);
			}
			if (playing) {
				$(this).addClass('active');
				show_content( incremented_index() );
			} else {
				$(this).removeClass('active');
			}
		},

		show_content = function (index) {
			$slideshow.css( { backgroundImage: $featured_infos.eq(index).css( 'background-image' ) } );
			$featured_infos.eq(content_index).fadeOut(fade_out_speed, function () {
				$featured_links.filter('.active').removeClass('active').addClass('visited').end().eq(index).addClass('active');
				if ( fade_in_index > 0 ) {
					$featured_infos.eq( fade_in_index ).stop().hide();
				}
				fade_in_index = index;
				$featured_infos.eq(index).fadeIn(fade_in_speed, function() {
					fade_in_index = -1;
				});
				content_index = index;
			});
			if (playing) {
				timeout = setTimeout(function () {
					show_content( incremented_index() );
				}, slide_milliseconds);
			}
			/* Trigger a custom event with the slide index */
			$(window).trigger('show_content', index);
		};

	/* Export this function so we can advance slides elsewhere; */
	jQuery.show_content = show_content;

	if ( $slideshow.data( 'slideMilliseconds' ) ) {
		slide_milliseconds = $slideshow.data( 'slideMilliseconds' );
	}

	if ( 'undefined' !== typeof $slideshow.data( 'fadeInSpeed' ) ) {
		fade_in_speed = $slideshow.data( 'fadeInSpeed' ); 
	}

	if ( 'undefined' !== typeof $slideshow.data( 'fadeOutSpeed' ) ) {
		fade_out_speed = $slideshow.data( 'fadeOutSpeed' ); 
	}

	// Crank it up
	if ( $slideshow.hasClass( 'manual' ) ) {
		show_content( 0 );
	} else {
		toggle_play();
		// Pause when not viewing
		$(window).focus(function () {
			toggle_play(true);
		}).blur(function () {
			toggle_play(false);
		});
	}

	// Handle clicks
	$featured_links.each(function (i) {
		$(this).click(function () {
			toggle_play(false);
			show_content(i);
			return false;
		});
	});

	$next_links.click( function() {
		show_content( incremented_index() );
		return false;
	});

	$prev_links.click( function() {
		show_content( incremented_index( -1 ) );
		return false;
	});
});
