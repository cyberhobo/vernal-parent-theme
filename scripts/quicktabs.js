jQuery( function($) {
	$('.quicktabs').each( function() {
		var container = $(this);
		container.find('> ul li a').click( function ( e ) {
			$(this).parent().addClass( 'active' ).siblings().removeClass( 'active' );
			container.find('.tab-panel').hide().filter(this.hash).show();
			e.preventDefault();
			return false;
		});
	});
});
