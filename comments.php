<?$i;?>

<?php if ( !empty($post->post_password) && $_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) : ?>
<p><?php _e('Enter your password to view comments.'); ?></p>
<?php return; endif; ?> 

<?php 
if ( function_exists( 'ckrating_get_comments' ) )
	$comments = ckrating_get_comments( 'status=approve&orderby=comment_karma&order=DESC&post_id=' . get_the_ID() );
?>

<?php if ( $comments ) : ?>
<h2><?php comments_number('','There is one comment:','There are % comments by other visitors:'); ?></h2>
<ul id="commentlist" class="commentlist">

<?php foreach ($comments as $comment) : ?>
	<li id="comment-<?php comment_ID() ?>" <?php comment_class(); ?> >
	<?php echo get_avatar( $comment, 64 ); ?>
	<div class="comment_text">
	<?php comment_text() ?>
	</div>
	<p><cite><?php comment_type(__('Response'), __('Trackback'), __('Pingback')); ?> <?php _e('shared by'); ?> <?php comment_author_link(); ?> &#8212; <?php comment_date() ?> @ <?php comment_time() ?></cite></p>
	<div class="vote">
	<?php if(function_exists(ckrating_display_karma)) { ckrating_display_karma(); }; ?>
	</div>
	</li>

<?php endforeach; ?>

</ul>

<?php else : // If there are no comments yet ?>

<?php endif; ?> 


<?php get_template_part("comment_new"); ?>

