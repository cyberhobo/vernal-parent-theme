<?php if ( comments_open() ) : ?>

<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
<p>You must be <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php the_permalink(); ?>">logged in</a> to post a comment.</p>
<?php else : ?>


<form id="commentform" action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post">
<h3>Add your comment:</h3>

<?php if ( $user_ID ) : ?>

<p>You are already logged in as <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?action=logout" title="<?php _e('Log out of this account') ?>">Logout &raquo;</a></p>

<p><textarea name="comment" id="comment" rows="6" tabindex="1"></textarea></p>
<?php else : ?>

<p><textarea name="comment" id="comment" rows="6" tabindex="1"></textarea></p>

<p><input type="text" name="author" id="author" value="<?php echo $comment_author; ?>" size="22" tabindex="2" />
<label for="author">Name <?php if ($req) _e('(required)'); ?></label></p>

<p><input type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" size="22" tabindex="3" />
<label for="email">Email (Required, but not published or shared) <?php if ($req) _e(''); ?></label></p>

<?php endif; ?>


<p><input name="submit" type="submit" id="submit" tabindex="5" class="button" value="Submit" />
<input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />
</p>
<?php do_action('comment_form', $post->ID); ?>

</form>
<div id="comment-done">
<p>Thanks very much for leaving a comment.</p>
</div>

<?php endif; // If registration required and not logged in ?>

<?php else : // Comments are closed ?>
<p><?php _e('Sorry, the comment form is closed at this time.'); ?></p>
<?php endif; ?>

